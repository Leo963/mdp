﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MDP
{
    public partial class Main : Form
    {
        GameManager gm = new GameManager();
        SaveManager sm = new SaveManager();
        LangManager lm = new LangManager();
        Label[] lbArr;
        Button[] btnArr;
        Config conf = new Config();
        public static string lang;
        public Main()
        {
            InitializeComponent();
        }
        private void Main_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            conf.readConfig();
            gm.LoadAssets();
            ShowMainMenu();
            GetUITranslation();
            sm.Load();
            UiUpdater.RunWorkerAsync();
            gm.startRtGame();
        }
        private void GetUITranslation()
        {
            foreach (Control control in this.Controls)
            {
                control.Text = lm.getString(control.Text, lang);
            }
        }
        private void btnToggle_Click(object sender, EventArgs e)
        {
            if (gm.rtRunning)
            {
                gm.pauseRtGame();
                btnToggle.Text = lm.getString("play", lang);
            }
            else
            {
                gm.resumeRtGame();
                btnToggle.Text = lm.getString("pause", lang);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void UiUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                lbMoney.Text = lm.getString("money", lang) + " " + gm.money.ToString() + "$";
            }
        }
        private void ShowMainMenu()
        {
            EnableUIControl(btnDepot);
            DisableUIControl(btnBack);
            DisableUIControl(btnBuy);
        }
        private void HideMainMenu()
        {
            DisableUIControl(btnDepot);
            EnableUIControl(btnBack);
        }
        private void DisableUIControl(Control control)
        {
            control.Hide();
            control.Enabled = false;
        }
        private void EnableUIControl(Control control)
        {
            control.Show();
            control.Enabled = true;
        }
        private void btnDepot_Click(object sender, EventArgs e)
        {
            HideMainMenu();
            EnableUIControl(btnBuy);
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            ShowMainMenu();
            if (lbArr != null)
            {
                for (int i = 0; i < lbArr.Length; i++)
                {
                    Controls.Remove(lbArr[i]);
                    Controls.Remove(btnArr[i]);
                }
                lbArr = null;
            }
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            lbArr = new Label[gm.buses.Count];
            btnArr = new Button[gm.buses.Count];
            Point loc = new Point(20, 100);
            for (int i = 0; i < lbArr.Length; i++)
            {
                lbArr[i] = new Label();
                lbArr[i].Location = loc;
                lbArr[i].Name = "lb" + gm.buses[i].name;
                lbArr[i].Text = gm.buses[i].name;
                btnArr[i] = new Button();
                btnArr[i].Location = new Point(loc.X + 200, loc.Y);
                btnArr[i].Name = "btn" + gm.buses[i].name;
                btnArr[i].Text = lm.getString("buy", lang);
                btnArr[i].Click += BuyBus;
                Controls.Add(lbArr[i]);
                Controls.Add(btnArr[i]);
                loc.Y += 30;
            }
        }

        private void BuyBus(object sender, EventArgs e)
        {
            Button thisButton = (Button)sender;
            string busName = thisButton.Name.Substring(3);
            gm.Buy(busName);
        }
    }
}
