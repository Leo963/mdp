﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MDP
{
    class Config
    {
        private void createConfig()
        {
            using (StreamWriter sw = new StreamWriter("settings.cfg"))
            {
                sw.WriteLine("Lang=en");
            }
        }
        public void readConfig()
        {
            if (File.Exists("settings.cfg"))
            {
                using (StreamReader sr = new StreamReader("settings.cfg"))
                {
                    string[] preset;
                    while(!sr.EndOfStream)
                    {
                        preset = sr.ReadLine().Split('=');
                        if (preset[0] == "Lang")
                        {
                            Main.lang = preset[1];
                        }
                    }
                }
            }
            else
            {
                createConfig();
            }
        }
    }
}
