﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MDP
{
    class Bus
    {
        public string name { get; set; }
        public string id { get; set; }
        public int price { get; set; }
        public int capacity { get; set; }
        public int owned { get; set; }
        public Bus()
        {}
        public Bus(string name, string id, int price, int capacity)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.capacity = capacity;
        }
    }
}
