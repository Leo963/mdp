﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MDP
{
    class SaveManager
    {
        string saveFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MDP";
        public void Load()
        {
            if (Directory.Exists(saveFolder))
            {
                if (File.Exists(saveFolder + @"\save.mdp"))
                {
                    using (StreamReader sr = new StreamReader("save.mdp"))
                    {
                        //Read save
                    }
                }
            }
            else
            {
                Directory.CreateDirectory(saveFolder);
            }
        }

        public void Save()
        {
            if (File.Exists(saveFolder + @"\save.mdp"))
            {
                //Do save
            }
            else
            {
                File.Create(saveFolder + @"\save.mdp");
            }
        }
    }
}
