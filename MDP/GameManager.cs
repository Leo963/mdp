﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
#pragma warning disable CS0618 // Type or member is obsolete
namespace MDP
{
    class GameManager
    {
        public float money = 1000000;
        public List<Bus> buses = new List<Bus>();
        Thread rtGame;
        static Main main = new Main();
        public bool rtRunning;

        public void LoadAssets()
        {
            BusLoad();
        }
        public void startRtGame()
        {
            rtRunning = false;
            rtGame = new Thread(new ThreadStart(this.realTimeGame));
            rtGame.IsBackground = true;
            rtGame.Start();
            rtGame.Suspend();
        }
        public void pauseRtGame()
        {
            rtGame.Suspend();
            rtRunning = false;
        }
        public void resumeRtGame()
        {
            rtGame.Resume();
            rtRunning = true;
        }
        private void realTimeGame()
        {
            while (true)
                {
                }
        }
        private void BusLoad()
        {
        string[] splitted = new string[2];
            try {
                string[] name = Directory.GetFiles(@"assets\buses", "*.mdp")
                                     .Select(Path.GetFileName)
                                     .ToArray();
                for (int row = 0; row < name.Length; row++)
                {
                    Bus bus = new Bus();
                    using (StreamReader sr = new StreamReader(@"assets\buses\" + name[row]))
                    {
                        do
                        {
                            splitted = sr.ReadLine().Split('=');
                            if ("id" == splitted[0])
                            {
                                bus.id = splitted[1];
                            }
                            else if ("name" == splitted[0])
                            {
                                bus.name = splitted[1];
                            }
                            else if ("price" == splitted[0])
                            {
                                if (!Int32.TryParse(splitted[1], out int price))
                                    System.Windows.Forms.MessageBox.Show("Price isn't a number", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                else
                                    bus.price = price;
                            }
                            else if ("cap" == splitted[0])
                            {
                                if (!Int32.TryParse(splitted[1], out int cap))
                                    System.Windows.Forms.MessageBox.Show("Capacity isn't a number", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                else
                                    bus.capacity = cap;
                            }
                        } while (!sr.EndOfStream);
                    }
                    bus.owned = 0;
                    buses.Add(bus);
                }
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Cannot find desired bus file", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                splitted[1] = "Bus file error";
            }
        }
        public void Buy(string name)
        {
			int busPrice = 0;
            int theBus = FindBus(name); //Do not use FindBus more than once
            busPrice = buses[theBus].price;
			if (busPrice < money)
			{
                int curramount = buses[theBus].owned;
                curramount++;
                money -= busPrice;
                buses[theBus].owned = curramount;

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("You do not have enough money", "Not enough money", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
            }
        }
        public int FindBus(string name)
        {
            return buses.FindIndex(x => x.name == name);
        }
    }
}
