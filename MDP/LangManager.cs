﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MDP
{
    class LangManager
    {
        
        public string getString(string translation, string language)
        {
            string[] splitted = new string[2];
            try
            {
                using (StreamReader sr = new StreamReader(@"lang\" + language + ".lang"))
                {
                    do
                    {
                        splitted = sr.ReadLine().Split('=');
                        if (sr.EndOfStream == true)
                        {
                            splitted[1] = "Language file error";
                            break;
                        }
                    } while (splitted[0] != translation);
                }
            }
            catch (Exception)
            {
                //System.Windows.Forms.MessageBox.Show("Cannot read language file", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                splitted = new string[2];
                splitted[1] = "Language file error";
            }
            return splitted[1];
        }
    }
}
